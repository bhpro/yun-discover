import RPi.GPIO as GPIO
import json
from SimpleWebSocketServer import SimpleWebSocketServer, WebSocket

clients = []
class SimpleEcho(WebSocket):
       
    def handleMessage(self):
        if self.address[0] == '127.0.0.1':
            # print "MENSAGEM INTERNA"
            for client in clients:
                client.sendMessage(self.data)
        else:
            print "MENSAGEM EXTERNA"
            payload = json.loads(self.data)
            stateOne = payload['ledOne'];
            print stateOne
            GPIO.setmode(GPIO.BCM)
            GPIO.setup(17, GPIO.OUT)
            GPIO.output(17,stateOne)
               
    def handleConnected(self):
        print self.address, 'connected'
        if self.address[0] != '127.0.0.1':
            clients.append(self)


    def handleClose(self):
        print self.address, 'closed'
        if self.address[0] != '127.0.0.1':
            clients.remove(self)

server = SimpleWebSocketServer('', 8000, SimpleEcho)
server.serveforever()
