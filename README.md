# README #

Spike com o Arduino YUN

### Implementações ###

* Arduino YUN, leitura de Luminusidade e Temperatura, envio dos dados para API (Python ou Java EE (REST))
* API REST em Java EE com Websockets e JAX-RS
* API REST em Python
* Websockets Server em Python
* UI em HTML e Java Script com Sockets
* Esquema Eléctrico Fritzing 


### Desenvolvido por ###
* Bruno Horta
* https://www.youtube.com/brunohorta
* http://bhtecnonerd.blogspot.pt/