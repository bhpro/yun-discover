package pt.essyun.bondary;


import pt.essyun.SensorDataNotification;

import javax.ejb.Stateless;
import javax.enterprise.event.Event;
import javax.inject.Inject;
import javax.ws.rs.POST;
import javax.ws.rs.Path;
import javax.ws.rs.core.Response;

@Stateless
@Path("notify")
public class SensorDataResource {

    @Inject
    @SensorDataNotification
    private Event<String> sensorDataEvent;

    @POST
    public Response postData(String sensorData) {
        sensorDataEvent.fire(sensorData);
        return Response.ok().build();
    }


}
