package pt.essyun.bondary;


import pt.essyun.SensorDataNotification;

import javax.ejb.Singleton;
import javax.enterprise.event.Observes;
import javax.websocket.OnClose;
import javax.websocket.OnOpen;
import javax.websocket.Session;
import javax.websocket.server.ServerEndpoint;
import java.util.concurrent.CopyOnWriteArraySet;


@Singleton
@ServerEndpoint("/notify")
public class SensorDataWebSocket {
    private CopyOnWriteArraySet<Session> sessionSet = new CopyOnWriteArraySet<>();


    @OnClose
    public void close(Session session) {
        sessionSet.remove(session);
    }


    @OnOpen
    public void open(Session session) {
        sessionSet.add(session);
    }

    public void onMessageApi(@Observes @SensorDataNotification String payload) {
        sessionSet.forEach(session -> session.getAsyncRemote().sendText(payload));
    }

}
