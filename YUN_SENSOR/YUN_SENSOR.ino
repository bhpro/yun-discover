#include <ArduinoJson.h>
#include <Process.h>
#define PHOTO_RESISTOR_PIN A0
#define TEMPERATURE_PIN A1
#define API_URL "http://192.168.0.100:5000/sensors"
#define UPDATE_DELAY 1000 //miliseconds

void setup() {
  Bridge.begin();
  //Iniciar a porta Serie para Debug
  Serial.begin(9600);
}

void loop() {
 double lux = processPhotoResistorData();
 double temp = processTemperatureData();
  postToAPI(temp, lux);
  delay(UPDATE_DELAY);
}
/**
 * SENSORES
 */
//Processa a leitura do Sensor de luz
double processPhotoResistorData(){
    int leitura_sensor = analogRead(PHOTO_RESISTOR_PIN); 
    return Light(leitura_sensor);
}
//Processa a leitura da Temperatura
double processTemperatureData(){
    return (5.0 * analogRead(TEMPERATURE_PIN) * 100.0) / 1024;
}

/**
 * API REST
 */
void postToAPI(double temperature, double lux) {  
  //Criar objeto json
  StaticJsonBuffer<200> jsonBuffer;
  JsonObject& root = jsonBuffer.createObject();
  root["temperature"] = temperature;
  root["lux"] = lux;
  
  String payload ;
  root.printTo(payload);
  
  // POST COMAND
  String cmd = "curl -d '"+String(payload)+"' -H \"Content-type: application/json\" -X POST "+API_URL;
 Serial.println(cmd);
  Process p;
  p.runShellCommand(cmd);
  Console.println(cmd);
  p.close();
}

/**
 * FUNÇOES DE CONVERÇÃO
 */
//Conversão da leitura ADC para LUX
double Light (int RawADC0){
  double Vout=RawADC0*0.0048828125;//Leitura_ADC*(VOLTAGEM_REF/1024)
  int lux=(2500/Vout-500)/10;//(500*VOLTAGEM_REF/Vout-500)/RESISTOR
  return lux;
}



