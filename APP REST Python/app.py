#!/usr/bin/python
import websocket
from flask import Flask, request
ws = websocket.WebSocket()
ws.connect("ws://127.0.0.1:8000")

app = Flask(__name__, static_url_path = "")

@app.route('/sensors', methods = ['POST'])
def create_task():
    payload =  request.data
    ws.send(payload)
    return payload

if __name__ == '__main__':
    app.run(debug = True, host = '0.0.0.0')

